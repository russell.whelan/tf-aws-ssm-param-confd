# tf-aws-ssm-param-confd

Terraform module to trigger reload of confd on parameter update

Gives you:

- Cloudwatch event rule for SSM Parameter Update
- Cloudwatch event target for SSM Document
- IAM Roles to allow running SSM on etc instances


## Contributing

Ensure any variables you add have a type and a description.
This README is generated with [terraform-docs](https://github.com/segmentio/terraform-docs):

`terraform-docs md . > README.md`

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| tag\_name | Name of the tag used for selection | string | `confd` | no |
| tag\_value | Value of the tag used for selection | string | `true` | no |

