data "aws_iam_policy_document" "ssm_parameter_trust" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "ssm_reload_confd" {
  statement {
    effect    = "Allow"
    actions   = ["ssm:SendCommand"]
    resources = ["arn:aws:ec2:${data.aws_region.current}:${data.aws_caller_identity.current.account_id}:instances/*"]
  }

  statement {
    effect    = "Allow"
    actions   = ["ssm:SendCommand"]
    resources = ["arn:aws:ssm:${data.aws_region.current}::document/AWS-RunShellScript"]
  }
}

resource "aws_iam_role_policy" "ssm_reload_confd" {
  name   = "SSMReloadConfd"
  role   = "${aws_iam_role.ssm_reload_confd.id}"
  policy = "${data.aws_iam_policy_document.ssm_reload_confd.json}"
}

resource "aws_iam_role" "ssm_reload_confd" {
  name               = "SSMReloadConfd"
  assume_role_policy = "${data.aws_caller_identity.current}"
}
