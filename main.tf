/**
 * # tf-aws-ssm-param-confd
 * 
 * Terraform module to trigger reload of confd on parameter update
 * 
 * Gives you:
 * 
 * - Cloudwatch event rule for SSM Parameter Update
 * - Cloudwatch event target for SSM Document
 * - IAM Roles to allow running SSM on etc instances
 * 
 * 
 * ## Contributing
 *
 * Ensure any variables you add have a type and a description.
 * This README is generated with [terraform-docs](https://github.com/segmentio/terraform-docs):
 *
 * `terraform-docs md . > README.md`
 */

resource "aws_cloudwatch_event_rule" "ssm_parameter_update" {
  name        = "capture-ssm-param-update"
  description = "Capture updates to parameters in the SSM Param Store"

  event_pattern = <<PATTERN

{
  "source": [
    "aws.ssm"
  ],
  "detail-type": [
    "Parameter Store Change"
  ]
}
PATTERN
}

resource "aws_cloudwatch_event_target" "ssm_reload_confd" {
  target_id = "ssm_reload_confd"
  arn       = "arn:aws:ssm:${var.aws_region}::document/AWS-RunShellScript"
  input     = "{\"commands\":[\"confd -onetime -backend ssm\"]}"
  rule      = "${aws_cloudwatch_event_rule.ssm_parameter_update.name}"
  role_arn  = "${aws_iam_role.ssm_confd_run.arn}"

  run_command_targets {
    key    = "tag:${var.tag_name}"
    values = ["${var.tag_value}"]
  }
}
