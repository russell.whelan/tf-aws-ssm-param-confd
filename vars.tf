variable "tag_name" {
  description = "Name of the tag used for selection"
  type        = "string"
  default     = "confd"
}

variable "tag_value" {
  description = "Value of the tag used for selection"
  type        = "string"
  default     = "true"
}
